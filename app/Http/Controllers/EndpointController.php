<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class EndpointController extends Controller
{
    public function procesarTexto(Request $request)
    {
        $texto = $request->input('texto');

        // Enviar el texto capturado a la API de ChatGPT
        $client = new Client([
            'base_uri' => 'https://api.openai.com/v1/',
        ]);

        $response = $client->request('POST', 'chat/completions', [
            'headers' => [
                'Authorization' => 'Bearer ' . env("OPENAI_API_KEY"),
                'Content-Type' => 'application/json'
            ],
            "json" => [
                "model" => "gpt-3.5-turbo",
                "messages" => [
                    ["role" => "user", "content" => $texto]
                ]
            ]
        ]);

        $respuestaGPT = json_decode($response->getBody(), true);
        $respuestaTexto = $respuestaGPT['choices'][0]['message']['content'];

        // Devolver la respuesta del modelo GPT como respuesta HTTP
        return response()->json(['respuesta' => $respuestaTexto]);
    }
}
